FROM nginx:stable-alpine
RUN wget https://www.tooplate.com/zip-templates/2136_kool_form_pack.zip && \
    unzip 2136_kool_form_pack.zip && \
    mv 2136_kool_form_pack/* /usr/share/nginx/html/
EXPOSE 80 

